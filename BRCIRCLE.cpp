#include<stdio.h>
#include<graphics.h>
#include<conio.h>
void circle1(int x0, int y0, int radius)
{
    int x =0;
    int y = radius;
    int d =3-2*radius;

    while (x <=y)
    {   setcolor(GREEN);
	putpixel(x0 + x, y0 + y, 7);
	putpixel(x0 + y, y0 + x, 7);
	putpixel(x0 - y, y0 + x, 7);
	putpixel(x0 - x, y0 + y, 7);
	putpixel(x0 - x, y0 - y, 7);
	putpixel(x0 - y, y0 - x, 7);
	putpixel(x0 + y, y0 - x, 7);
	putpixel(x0 + x, y0 - y, 7);
  x++;
	if (d< 0)
	{

	    d =d+4*x+6;
	}

	else
	{

	    d= d+4*(x-y) +10;
	    y=y-1;
	}

    }
}

int main()
{
	int gdriver=DETECT, gmode, error, x, y, r;
	initgraph(&gdriver, &gmode, "c:\\turboc3\\bgi");

	printf("BRESENHAMS Circle");


	printf("\n\n\nEnter radius of circle: ");
	scanf("%d", &r);

	printf("Enter co-ordinates of center(x and y): ");
	scanf("%d%d", &x, &y);
	circle1(x, y, r);


	getch();
	return 0;
}
