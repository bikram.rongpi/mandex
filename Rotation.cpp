
#include<stdio.h>
#include<conio.h>
#include<graphics.h>
#include<math.h>


void triangle(int x1, int y1,int x2,int y2, int x3, int y3);
void rotate(int x1, int y1, int x2, int y2, int x3, int y4);
void main()
{
 int gd=DETECT,gm;
 int x1,y1,x2,y2,x3,y3;
 initgraph(&gd,&gm,"C:\\TURBOC3\\BGI");
 printf("ROTATION OF A TRIANGLE");	


 printf("\n\n\nEnter the 1st point of the triangle:\n");
 scanf("%d%d",&x1,&y1);
 printf("Enter the 2nd point of the triangle:\n");
 scanf("%d%d",&x2,&y2);
 printf("Enter the 3rd point of the triangle:\n");
 scanf("%d%d",&x3,&y3);
 cleardevice();
 triangle(x1,y1,x2,y2,x3,y3);
 getch();
 cleardevice();
 rotate(x1,y1,x2,y2,x3,y3);
 getch();
 }

 void triangle(int x1, int y1, int x2, int y2, int x3, int y3)
 {
  line(x1,y1,x2,y2);
  line(x2,y2,x3,y3);
  line(x3,y3,x1,y1);
 }
void rotate(int x1, int y1, int x2, int y2, int x3, int y3)
{
 int x,y,a1,b1,a2,b2,a3,b3,p=x2,q=y2;
 float Angle;
 printf("Enter the angle of rotation :\n");
 scanf("%f",&Angle);
 a1=p+(x1-p)*cos(Angle)-(y1-q)*sin(Angle);
 b1=p+(x1-p)*sin(Angle)-(y1-q)*cos(Angle);
 a2=p+(x2-p)*cos(Angle)-(y2-q)*sin(Angle);
 b2=p+(x2-p)*sin(Angle)-(y2-q)*cos(Angle);
 a3=p+(x3-p)*cos(Angle)-(y3-q)*sin(Angle);
 b3=p+(x3-p)*sin(Angle)-(y3-q)*cos(Angle);
 printf("rotated");
 setcolor(GREEN);
 triangle(a1,b1,a2,b2,a3,b3);
 getch();
 }

