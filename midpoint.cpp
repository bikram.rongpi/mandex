
#include<stdio.h>
#include<graphics.h>
#include<conio.h>
void drawcircle(int x0, int y0, int radius)
{
    int x = radius;
    int y = 0;
    int p =5/4-radius;

    while (x >= y)
    {   setcolor(GREEN);
	putpixel(x0 + x, y0 + y, 7);
	putpixel(x0 + y, y0 + x, 7);
	putpixel(x0 - y, y0 + x, 7);
	putpixel(x0 - x, y0 + y, 7);
	putpixel(x0 - x, y0 - y, 7);
	putpixel(x0 - y, y0 - x, 7);
	putpixel(x0 + y, y0 - x, 7);
	putpixel(x0 + x, y0 - y, 7);

	if (p<= 0)
	{
	    y =y+ 1;
	    p =p+2*y + 3;
	}

	else
	{
	    x =x- 1;

	    p= p-2*x + 5;

	}

    }
}

int main()
{
	int gdriver=DETECT, gmode, error, x, y, r;
	initgraph(&gdriver, &gmode, "c:\\turboc3\\bgi");


	printf("MIDPOINT OF A CIRCLE");

	printf("\n\n\nEnter radius of circle: ");
	scanf("%d", &r);

	printf("Enter co-ordinates of center(x and y): ");
	scanf("%d%d", &x, &y);
	drawcircle(x, y, r);


	getch();
	return 0;
}
